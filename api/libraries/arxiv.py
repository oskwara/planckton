from ..objects import searchQueryObject
from ..objects import searchResultObject
import urllib
import web
import xml.dom.minidom
import itertools

APIurl = 'http://export.arxiv.org/api/query?search_query='
simpleSearch = False
firstItem = True
	
def search(searchinput, sQueryObj):
	return generateHTML(searchinput,generateResult(getAPIResult(sQueryObj)))

def getAPIResult(myURL):
	global APIurl
	url = APIurl + getQuery(myURL)
	return urllib.urlopen(url).read()

def getQuery(mobject):
	global firstItem
	mquery = ''
	if mobject.query is not None:
		mquery += 'all:'+ mobject.query
		firstItem = False
	if mobject.title is not None:
		mquery += processList(mobject.title, 'ti:')
	if mobject.author is not None:
		mquery += processList(mobject.author, 'au:')
	if mobject.abstract is not None:
		mquery += processList(mobject.abstract, 'abs:')
	if mobject.comment is not None:
		mquery += processList(mobject.comment, 'co:')
	mquery += '&start=0&max_results=10'
	firstItem = True
	return mquery

def processList(mlist, type):
	global firstItem
	mquery = ""
	if firstItem == False:
		for i in mlist:
			mquery += '+AND+' + type + i
	else:
		firstItem = False
		first = True
		for i in mlist:
			if first == True:
				first = False
				mquery += type + i
			else:	
				mquery += '+AND+' + type + i
	return mquery
	
def generateResult(input):
	mobject = searchResultObject.searchResultObject()
	xmldoc = xml.dom.minidom.parseString(input)
	entries = xmldoc.getElementsByTagName("entry")
	if entries:
		for entry in entries:
			mobject.origin = "Arxiv"
			mobject.title = getText(entry.getElementsByTagName("title")[0].childNodes)
			mobject.id = (getText(entry.getElementsByTagName("id")[0].childNodes))
			mobject.summary = getText(entry.getElementsByTagName("summary")[0].childNodes)
			mobject.author = getText(entry.getElementsByTagName("name")[0].childNodes)
			mobject.updated = (getText(entry.getElementsByTagName("updated")[0].childNodes)).replace("Z","")
	return mobject

def generateHTML(searchinput, input):
	output = ''
	if input.origin is None:
		output += '<div class="entry">'
		output += '<div class="entry_keywords"><i>No entries found for: "%s"</i></div>' % searchinput
		output += '</div>'
	else:
		for a,b,c,d,e,f in itertools.izip(input.origin, input.title, input.id, input.summary,input.author,input.updated):
			output += '<div class="entry">'
			output += '<div class="entry_titlewrapper">'
			output += '<div class="entry_icon"><a href="%s"><img src="static/img/arxiv.png"></a></div>' % c.replace("/abs/","/pdf/")
			output += '<div class="entry_title"><a href="%s' % c
			output += '">%s' % b
			output += '</a></div>'
			output += '<div class="entry_keywords"><i>Author: %s</i></div>' % e
			output += '</div>'
			output += '<div class="entry_descwrapper">'
			output += '<div class="entry_desc"><u>Abstract:</u> %s' % d
			output += '</div></div>'
			output += '<div class="entry_detailswrapper">'
			output += '<div class="entry_details"><i>Last submission: %s</i>' % f
			output += '</div></div>'
			output += '</div></div>'
	return output
	
def getText(nodelist):
    rc = []
    for node in nodelist:
        if node.nodeType == node.TEXT_NODE:
            rc.append(node.data)
    return ''.join(rc)
    
#TEST
#def testMe(value):
#	output = getResult(getQuery(value))
#	print output

#test = object.searchQueryObject()
#test.title = "planck"
#test.title = "einstein"
#test.author = "Wolf"
#testMe(test)
	