from ..objects import searchQueryObject
from ..objects import searchResultObject
import urllib
import web
import xml.dom.minidom
import itertools

APIurl = 'http://ieeexplore.ieee.org/gateway/ipsSearch.jsp?'
simpleSearch = False
firstItem = True
	
def search(searchinput, sQueryObj):
	return generateHTML(searchinput,generateResult(getAPIResult(sQueryObj)))

def getAPIResult(myURL):
	global APIurl
	url = APIurl + getQuery(myURL)
	return urllib.urlopen(url).read()

def getQuery(mobject):
	global firstItem
	mquery = ''
	if mobject.query is not None:
		mquery += 'querytext='+ mobject.query
		firstItem = False
	if mobject.title is not None:
		mquery += processList(mobject.title, 'ti=')
	if mobject.author is not None:
		mquery += processList(mobject.author, 'au=')
	if mobject.abstract is not None:
		mquery += processList(mobject.abstract, 'ab=')
	#if mobject.comment is not None:
	#	mquery += processList(mobject.comment, 'co:')
	mquery += '&hc=10'
	firstItem = True
	return mquery

def processList(mlist, type):
	global firstItem
	mquery = ""
	if firstItem == False:
		for i in mlist:
			mquery += '&' + type + i
	else:
		firstItem = False
		first = True
		for i in mlist:
			if first == True:
				first = False
				mquery += type + i
			else:	
				mquery += '&' + type + i
	return mquery
	
def generateResult(input):
	mobject = searchResultObject.searchResultObject()
	xmldoc = xml.dom.minidom.parseString(input)
	entries = xmldoc.getElementsByTagName("document")
	if entries:
		for entry in entries:
			mobject.origin = "IEEE"
			mobject.title = getText(entry.getElementsByTagName("title")[0].childNodes)
			mobject.id = (getText(entry.getElementsByTagName("pdf")[0].childNodes))
			#quick fix if there is no abstract tag (seriously IEEE...consistency?)
			if entry.getElementsByTagName("abstract"):
				mobject.summary = getText(entry.getElementsByTagName("abstract")[0].childNodes)
			else:
				mobject.summary = ""
			mobject.author = getText(entry.getElementsByTagName("authors")[0].childNodes)
			mobject.updated = (getText(entry.getElementsByTagName("py")[0].childNodes))
	return mobject

def generateHTML(searchinput, input):
	output = ''
	if input.origin is None:
		output += '<div class="entry">'
		output += '<div class="entry_keywords"><i>No entries found for: "%s"</i></div>' % searchinput
		output += '</div>'
	else:
		for a,b,c,d,e,f in itertools.izip(input.origin, input.title, input.id, input.summary,input.author,input.updated):
			output += '<div class="entry">'
			output += '<div class="entry_titlewrapper">'
			output += '<div class="entry_icon"><a href="%s"><img src="static/img/ieee.png"></a></div>' % c
			output += '<div class="entry_title"><a href="%s' % c
			output += '">%s' % b
			output += '</a></div>'
			output += '<div class="entry_keywords"><i>Author: %s</i></div>' % e
			output += '</div>'
			output += '<div class="entry_descwrapper">'
			output += '<div class="entry_desc"><u>Abstract:</u> %s' % d
			output += '</div></div>'
			output += '<div class="entry_detailswrapper">'
			output += '<div class="entry_details"><i>Last submission: %s</i>' % f
			output += '</div></div>'
			output += '</div></div>'
	return output
	
def getText(nodelist):
    rc = []
    for node in nodelist:
        if node.nodeType == node.CDATA_SECTION_NODE:
        	rc.append(node.data)
    return ''.join(rc)
    
#TEST
def testMe(value):
	output = search("planck", value)
	print output

#test = searchQueryObject.searchQueryObject()
#test.query = "appel"
#test.title = "planck"
#test.title = "einstein"
#test.author = "Wolf"
#testMe(test)
	