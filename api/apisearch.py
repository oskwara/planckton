from objects import searchQueryObject
from libraries import arxiv
from libraries import ieee
from libraries import nature
import bleach


#Search requestor; example: search("pandas","arxiv") will search for pandas in the arxiv library
#The searchinput will be sanitized first through the bleach.clean() method

def search(searchinput, cAPI):
	searchinput = bleach.clean(searchinput)
	if cAPI == "arxiv":
		return arxiv.search(searchinput, processEntry(searchinput))
#	elif cAPI == "ieee":
#		return ieee.search(searchinput, processEntry(searchinput))
	elif cAPI == "nature":
		return nature.search(searchinput, processEntry(searchinput))
	else:
		return arxiv.search(searchinput, processEntry(searchinput))
	
#Searchinput processor; This will convert the searchquery to an universal search query object model which can 
#be used by the API call methods; example: processEntry("alpha;Title: beta") will process the input as 
#search on ANY entry with "alpha" AND Title entry "beta"
def processEntry(searchinput):
	mobject = searchQueryObject.searchQueryObject()
	mlist = searchinput.split(';')
	for i in mlist:
		i = i.lstrip(' ')
		if i[:7] == "Author:":
			mobject.author = i[7:]
		elif i[:6] == "Title:":
			mobject.title = i[6:]
		elif i[:9] == "Abstract:":
			mobject.abstract = i[9:]
		elif i[:8] == "Comment:":
			mobject.comment = i[8:]
		else:
			mobject.query = i
	return mobject
	
#Test method for quick 'n' dirty testing
#def testME():
#	q = 'Author: beta; negative result'
#	print q
#	print (processEntry(q).title)
#	print arxiv.search2(processEntry(q))
	
#testME()
