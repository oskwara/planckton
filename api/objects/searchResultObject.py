class searchResultObject(object):
	def __init__(self):
		self._origins = None
		self._ids = None
		self._titles = None
		self._authors = None
		self._summaries = None
		self._updateds = None
	#	self._jrefs = []
	#	self._scats = []
	#	self._rns = []
	#	self._ids = []
	
	#SETTER	
	def setid(self, value):
		if self._ids is None:
			self._ids = []
		self._ids.append(value)
	#GETTER
	def getid(self):
		return self._ids
	#DEL
	def delid(self):
		self._ids = None
		
	def setorigin(self, value):
		if self._origins is None:
			self._origins = []
		self._origins.append(value)
		
	def getorigin(self):
		return self._origins
		
	def delorigin(self):
		self._origins = None
		
	def settitle(self, value):
		if self._titles is None:
			self._titles = []
		self._titles.append(value)
		
	def gettitle(self):
		return self._titles
		
	def deltitle(self):
		self._titles = None
		
	def setauthor(self, value):
		if self._authors is None:
			self._authors = []
		self._authors.append(value)
		
	def getauthor(self):
		return self._authors
		
	def delauthor(self):
		self._authors = None
		
	def setsummary(self, value):
		if self._summaries is None:
			self._summaries = []
		self._summaries.append(value)
		
	def getsummary(self):
		return self._summaries
		
	def delsummary(self):
		self._summaries = None

	def setupdated(self, value):
		if self._updateds is None:
			self._updateds = []
		self._updateds.append(value)
		
	def getupdated(self):
		return self._updateds
		
	def delupdated(self):
		self._updateds = None
		
	#def addcomment(self, comment):
	#	self.comments.append(comment)

	#def addjref(self, jref):
	#	self.jrefs.append(jref)
		
	#def addscat(self, scat):
	#	self.scats.append(scat)
		
	#def addrn(self, rn):
	#	self.rns.append(rn)
		
	#def addid(self, id):
	#	self.ids.append(id)
					
	#PROPERTIES
	origin = property(getorigin, setorigin, delorigin, "from which API (Arxiv, Nature,...)")
	id = property(getid, setid, delid, "id of result")
	title = property(gettitle, settitle, deltitle, "title of result")
	author = property(getauthor, setauthor, delauthor, "author of result")
	summary = property(getsummary, setsummary, delsummary, "summary of result")
	updated = property(getupdated, setupdated, delupdated, "last updated")

#TEST	
#f = searchQueryObject()
#f.title = "test"
#print f.title
#del f.title