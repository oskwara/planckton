class searchQueryObject(object):

	def __init__(self):
		self._query = None
		self._titles = None
		self._authors = None
		self._abstracts = None
		self._comments = None
	#	self._jrefs = []
	#	self._scats = []
	#	self._rns = []
	#	self._ids = []
	
	#SETTER	
	def setquery(self, value):
		self._query = value
	#GETTER
	def getquery(self):
		return self._query
	#DEL
	def delquery(self):
		self._query = ""
		
	def settitle(self, value):
		if self._titles is None:
			self._titles = []
		self._titles.append(value)
		
	def gettitle(self):
		return self._titles
		
	def deltitle(self):
		self._titles = None
		
	def setauthor(self, value):
		if self._authors is None:
			self._authors = []
		self._authors.append(value)
		
	def getauthor(self):
		return self._authors
		
	def delauthor(self):
		self._authors = None
		
	def setabstract(self, value):
		if self._abstracts is None:
			self._abstracts = []
		self._abstracts.append(value)
		
	def getabstract(self):
		return self._abstracts
		
	def delabstract(self):
		self._abstracts = None

	def setcomment(self, value):
		if self._comments is None:
			self._comments = []
		self._comments.append(value)
		
	def getcomment(self):
		return self._comments
		
	def delcomment(self):
		self._comments = None
		
	#def addcomment(self, comment):
	#	self.comments.append(comment)

	#def addjref(self, jref):
	#	self.jrefs.append(jref)
		
	#def addscat(self, scat):
	#	self.scats.append(scat)
		
	#def addrn(self, rn):
	#	self.rns.append(rn)
		
	#def addid(self, id):
	#	self.ids.append(id)
					
	#PROPERTIES
	query = property(getquery, setquery, delquery, "query value for simple search")
	title = property(gettitle, settitle, deltitle, "values for 'title' search entry")
	author = property(getauthor, setauthor, delauthor, "values for 'author' search entry")
	abstract = property(getabstract, setabstract, delabstract, "values for 'abstract' search entry")
	comment = property(getcomment, setcomment, delcomment, "values for 'comment' search entry")

#TEST	
#f = searchQueryObject()
#f.title = "test"
#print f.title
#del f.title