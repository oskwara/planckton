//advanced search input concatenation
function validate() {
	var any = document.getElementById('a_any').value;
	var title = document.getElementById('a_title').value;
	var author = document.getElementById('a_author').value;
	var abs = document.getElementById('a_abstract').value;
	
	var output = "";
	
	if (any != "") {output = any}
	if (title != "") {if (output != "") {output = output + ";Title: " + title} else {output = output + "Title: " + title}}
	if (author != "") {if (output != "") {output = output + ";Author: " + author} else {output = output + "Author: " + author}}
	if (abs != "") {if (output != "") {output = output + ";Abstract: " + abs} else {output = output + "Abstract: " + abs}}
	submitform(output);
}

//Temporary hack - disabled all advanced fields, put everything in new regular "q" field, needs better solution - OSK
function submitform(entry)
{
  var container = document.getElementById("a_form");
  var input = document.createElement("input");
  input.name = "q";
  input.value = entry;
  input.type = "hidden";
  container.appendChild(input);
  document.getElementById("a_any").disabled = "disabled";
  document.getElementById("a_title").disabled = "disabled";
  document.getElementById("a_author").disabled = "disabled";
  document.getElementById("a_abstract").disabled = "disabled";
}