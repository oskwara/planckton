import web
import xml.dom.minidom
import json

def get(q):
	if not q:
		return []
	pq = [t.strip() for t in q.split(' ') if len(t.strip())!=0]
	pq = '%' + '%'.join(pq) + '%'
	return pq
	
def generateJSON(input):
	xmldoc = xml.dom.minidom.parseString(input)
	entries = xmldoc.getElementsByTagName("entry")
	output = handleEntries(entries)
	return output

def getText(nodelist):
    rc = []
    for node in nodelist:
        if node.nodeType == node.TEXT_NODE:
            rc.append(node.data)
    return ''.join(rc)

def handleEntries(entries):
	myOut = '{"Result":['
	if entries:
		for entry in entries:
			myOut += handleEntry(entry)
	else:
		myOut += '{"Title":"No matches were found","url":""},'
	myOut = myOut[:-1]
	myOut += ']}'
	return myOut
	
def handleEntry(entry):
	myOut = ''
	title = entry.getElementsByTagName("title")[0]
	id = entry.getElementsByTagName("id")[0]
	summary = entry.getElementsByTagName("summary")[0]
	myOut += '{'
	myOut += '"Title":"%s",' % json.dumps(getText(title.childNodes))
	myOut += '"url":"%s"' % json.dumps(getText(id.childNodes))
	"""myOut += '"summary":"%s"' % getText(summary.childNodes)"""
	myOut += '},'
	return myOut