#!/usr/bin/env python
# -*- coding: utf-8 -*-

import urllib, sys
import web, settings
import search
import msearch
import about
from api import apisearch
#import mAPI.mapi

#web.config.debug = False

urls = (
    '^/$', 'do_index',						
    '^/about[/]?$', 'do_about',			
    '^/search[/]?$', 'do_search',	
    '^/advanced[/]?$', 'do_advanced',
    '^/mAPI/msearch[/]?$', 'do_msearch'
)

app = web.application(urls, globals())
render = web.template.render(settings.TEMPLATE_FOLDER)

class do_index:    
    def GET(self):
        return render.index('Planckton - Scientific Search Engine')

class do_about:
	def GET(self):
		return render.about(title='About Planckton')
		
class do_advanced:
	def GET(self):
		input = web.input(q='')
		title='Planckton - Advanced Search'
		return render.advanced(title,input.cAPI)
		
class do_search:
	def GET(self):
		input = web.input(q='')
		input.q = (input.q).encode('ascii',errors='ignore')
		output = apisearch.search(input.q, input.cAPI)
		return render.search(output,input.q,input.cAPI)
		
class do_msearch:
	def GET(self):
		input = web.input(q='')
		searchinput = input.q
		url = 'http://export.arxiv.org/api/query?search_query=all:' + searchinput + '&start=0&max_results=5'
		output = msearch.generateJSON(urllib.urlopen(url).read())
		return render.msearch(output)

if __name__ == "__main__":
    web.wsgi.runwsgi = lambda func, addr=None: web.wsgi.runfcgi(func, addr)
    app.run()
