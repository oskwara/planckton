def get(q):
	if not q:
		return []
	pq = [t.strip() for t in q.split(' ') if len(t.strip())!=0]
	pq = '%' + '%'.join(pq) + '%'
	return pq