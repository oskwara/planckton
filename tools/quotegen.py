from sqlalchemy import *
from sqlalchemy.orm import sessionmaker

db = create_engine(
    "postgresql+psycopg2://example:example@localhost/example",
    isolation_level="READ UNCOMMITTED"
)

db.echo = False
metadata = MetaData(db)
quotes = Table('quotes', metadata, autoload=True)
Session = sessionmaker(bind=db)
session = Session()

class Quotes(object):
    def __init__(self, qtext=None, qauthor=None):
        self.qtext = qtext
        self.qauthor = qauthor
    def __repr__(self):
        return self.qtext

quotemapper = mapper(Quotes, quotes)
mike = session.query(Quotes).get_by(qtext='test')
print mike

def run(stmt):
    rs = stmt.execute()
    for row in rs:
        print row
 
def test():
	conn = engine.connect()
	#mtest = conn.execute("SELECT quote_text FROM public.quotes WHERE quote_id = '0'")
	#mtest = select([quotes.c.quote_text]).where(quotes.c.quote_id.match('0', postgresql_regconfig='english'))
	mtest = select([quotes.c.quote_text],quotes.c.quote_id == '0')
	return run(mtest)
		
#print test()
