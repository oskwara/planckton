import web
import webtest

# Init our application, this is just about the most basic setup
urls = ('/', 'Index')
urls = ('/(.*)', 'Index')
app = web.application(urls, globals())

class Index:
    # GET method is used when there is no form data sent to the server
    def GET(self, name):
        if(not name):
            name='John Doe'

        return 'Hello, ' + name

application = app.wsgifunc()

# This is a test function. We're using WebTest library
# We check if a change in the URL corresponds to a desired change
# in the greeting. We also check server's return code
def run_tests():
    # Run WebTest server
    middleware = []
    testApp = webtest.TestApp(app.wsgifunc(*middleware))

    # Default name is John Doe
    r = testApp.get('/')
    assert r.status == '200 OK'
    r.mustcontain('Hello, John Doe')

    # Check if the name changes
    r = testApp.get('/Jack Strong')
    assert r.status == '200 OK'
    r.mustcontain('Hello, Jack Strong')

    print 'All tests completed'

if __name__=="__main__":
    run_tests()